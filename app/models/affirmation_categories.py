from app import db

affirmation_categories = db.Table(
    'affirmation_categories',
    db.Column('id', db.Integer, primary_key=True),
    db.Column('affirmation_id', db.Integer, db.ForeignKey('affirmation.id')),
    db.Column('category_id', db.Integer, db.ForeignKey('category.id'))
)
