from app import db
from datetime import datetime
from pytz import timezone
from app.models.affirmation_categories import affirmation_categories
from app.models.category import Category


class Affirmation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(255), nullable=False)
    created_at = db.Column(db.DateTime(timezone=True), default=datetime.now(tz=timezone('Asia/Tokyo')))
    subscriptions = db.relationship('Category', secondary=affirmation_categories)

    @classmethod
    def from_args(cls, content, created_at):
        affirmation = cls()
        affirmation.content = content
        affirmation.created_at = created_at
        return affirmation

    def to_dict(self):
        return {
            'id': self.id,
            'content': self.content,
            'created_at': self.created_at,
            'categories': [Category.to_dict(c) for c in self.subscriptions]
        }
