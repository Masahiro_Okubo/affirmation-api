from app import db


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)

    @classmethod
    def from_args(cls, name):
        category = cls()
        category.name = name
        return category

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name
        }
