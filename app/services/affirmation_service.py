from app import db
from datetime import datetime

from app.models.affirmation import Affirmation


def add_category(content, created_at):
    u = Affirmation.from_args(content, created_at)
    db.session.add(u)
    db.session.commit()


def find_one(affirmation_id):
    return Affirmation.query.filter_by(id=affirmation_id)[0]
