from app import db
from app.models.category import Category


def add_category(name):
    u = Category.from_args(name)
    db.session.add(u)
    db.session.commit()


def find_one(category_id):
    return Category.query.filter_by(id=category_id)[0]
