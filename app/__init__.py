# -*- coding: utf-8 -*-

import os
from logging import INFO

from flask.app import Flask
from flask.helpers import make_response, url_for
from flask.json import jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.logger.setLevel(INFO)

# app.config.from_object('config.default')
# app.config.from_pyfile('config.py')
app.config['SQLALCHEMY_DATABASE_URI'] = \
    'mysql+pymysql://Masahiro:masahiro4180@Masahiro.mysql.pythonanywhere-services.com/Masahiro$affirmation_db'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:@localhost/flask-affirmation'
app.config['SECRET_KEY'] = 'f76bafcccd4c1135f37f06e818cad3cc30e6d64a3c6c6653'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

# models
from app.models.category import Category
from app.models.affirmation import Affirmation
from app.models.affirmation_categories import affirmation_categories
from app.models.exceptions import BadRequest, Unauthorized, Forbidden

# views
from app.views.category import category
from app.views.affirmation import affirmation

# viewsのルートの設定
app.register_blueprint(category, url_prefix='/api/v1/category')
app.register_blueprint(affirmation, url_prefix='/api/v1/affirmation')


@app.errorhandler(401)
def handler_unauthorized(error):
    return make_response(jsonify({'message': 'Unauthorized'}), 401)


@app.errorhandler(404)
def handle_not_found(error):
    return make_response(jsonify({'message': 'Not Found'}), 404)


@app.errorhandler(500)
def handle_internal_server_error(error):
    return make_response(jsonify({'message': 'Internal Server Error'}), 500)


@app.errorhandler(BadRequest)
def handle_bad_request_exception(error):
    return make_response(jsonify(error.to_dict()), error.status_code)


@app.errorhandler(Unauthorized)
def handle_unauthorized_exception(error):
    return make_response(jsonify(error.to_dict()), error.status_code)


@app.errorhandler(Forbidden)
def handle_forbidden_exception(error):
    return make_response(jsonify(error.to_dict()), error.status_code)


def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


@app.route("/site-map")
def site_map():
    links = []
    for rule in app.url_map.iter_rules():
        if "GET" in rule.methods and has_no_empty_params(rule):
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links.append((url, rule.endpoint))
    return jsonify({
        "response": links
    })
