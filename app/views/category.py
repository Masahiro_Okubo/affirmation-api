from flask.blueprints import Blueprint
from flask.globals import request
from flask.json import jsonify

from app import db
from app.models.category import Category
from app.services import category_service

category = Blueprint('category', __name__)


@category.route('', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        data = request.get_json()
        if data['data']['name']:
            category_service.add_category(data['data']['name'])
            return jsonify('News was created!')
        else:
            return jsonify('Missing param')

    category = [Category.to_dict(n) for n in Category.query.all()]
    return jsonify({'data': category})


@category.route('/<category_id>', methods=['GET', 'PUT'])
def show(category_id):
    data = request.get_json()
    if request.method == 'PUT':
        if data['data']['name']:
            category = category_service.find_one(category_id)
            category.name = data['data']['name']
            db.session.add(category)
            db.session.commit()
            return jsonify('Successfully updated!')
        else:
            return jsonify('faild')
    category = category_service.find_one(category_id)
    return jsonify({
        'data': Category.to_dict(category)
    })


@category.route('/delete/<category_id>', methods=['DELETE'])
def delete(category_id):
    category = category_service.find_one(category_id)
    db.session.delete(category)
    db.session.commit()
    return jsonify('Successfully deleted!!')
