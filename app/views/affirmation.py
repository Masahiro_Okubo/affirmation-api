from flask.blueprints import Blueprint
from flask.globals import request
from flask.json import jsonify
from datetime import datetime
from app import db
from app.models.affirmation import Affirmation
from app.models.category import Category
from app.services import affirmation_service, category_service

affirmation = Blueprint('affirmation', __name__)


@affirmation.route('', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        data = request.get_json()
        if data['data']['content']:
            affirmation_service.add_category(data['data']['content'], datetime.now())
            return jsonify('News was created!')
        else:
            return jsonify('Missing param')

    affirmation = [Affirmation.to_dict(n) for n in Affirmation.query.all()]
    return jsonify({'data': affirmation})


@affirmation.route('/<affirmation_id>', methods=['GET', 'PUT'])
def show(affirmation_id):
    if request.method == 'PUT':
        data = request.get_json()
        if data['data']['content']:
            if data['data']['category']:
                affirmation = affirmation_service.find_one(affirmation_id)
                category = category_service.find_one(data['data']['category'])
                affirmation.subscriptions.append(category)
                db.session.commit()
                return jsonify('Successfully added category!')
            affirmation = affirmation_service.find_one(affirmation_id)
            affirmation.content = data['data']['content']
            db.session.add(affirmation)
            db.session.commit()
            return jsonify('Successfully updated!')
        else:
            return jsonify('faild')
    affirmation = affirmation_service.find_one(affirmation_id)
    return jsonify({
        'data': Affirmation.to_dict(affirmation)
    })


@affirmation.route('/delete/<affirmation_id>', methods=['DELETE'])
def delete(affirmation_id):
    affirmation = affirmation_service.find_one(affirmation_id)
    db.session.delete(affirmation)
    db.session.commit()
    return jsonify('Successfully deleted!!')
