import os

DEBUG = False
SECRET_KEY = ''
# SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:@localhost/flask-affirmation'
# SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
SQLALCHEMY_TRACK_MODIFICATIONS = False
